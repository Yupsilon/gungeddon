﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class propertyWeapon
{
	public const int angleDeltaBase=40;

	public enum WeaponType {
		generic_Bullet=0,
		tank_cannon=1,
		land_Mines=2
	}
	public int level=0;

	public WeaponType wType = WeaponType.generic_Bullet;
	public int Clip=0;
	public void Fire (entityPlayer launcher,Vector2 center, float angle, int turretID)
	{
		if (Clip > 0) {
			float radStart = 0;
			float radDelta = 0;
			float radAdd = angleDeltaBase * GetProjectileSpreadMultiplier ();

			if (GetNumProjectiles () > 1) {
				radStart = radAdd * (GetNumProjectiles ()-1) / 2f;
				radDelta = radAdd;
			}

			for (int iP = 0; iP < GetNumProjectiles (); iP++) {
				entityProjectile Zim;

				switch (wType) {
				case WeaponType.generic_Bullet:
				case WeaponType.tank_cannon:
					Zim = entityProjectile.LaunchProjectile ((wType == WeaponType.tank_cannon ? "bullet_tank" : "bullet_default"), launcher,
						new Vector3 (center.x, center.y, -5.5f),
						GetRange () / GetProjectileVelocity (),
						GetDamageMultiplier () * launcher.ModifierProperties [propertyModifier.property_outgoingdamage],
						GetProjectileVelocity(),
						360 + 90 - angle + radStart - radDelta * iP + UnityEngine.Random.Range (-100, 100) / 100f * GetAccuracy (),
						launcher.side);

					if (level > 0) {
							GameObject.Destroy(Zim.gameObject.GetComponent<AudioSource>());
						}
						if (Zim.GetComponent<ParticleSystem>() != null)
						{
							ParticleSystem.MainModule pmain = Zim.GetComponent<ParticleSystem>().main;
							pmain.startColor = Zim.GetComponent<SpriteRenderer>().color;
						}

						break;
				case WeaponType.land_Mines:
					Zim = entityProjectile.LaunchProjectile ("landmine", launcher,
						new Vector3 (center.x, center.y, -0.5f),
						12,
						GetDamageMultiplier () * launcher.ModifierProperties [propertyModifier.property_outgoingdamage],
						GetProjectileVelocity(),
						360 + 90 - angle + radStart - radDelta * iP + UnityEngine.Random.Range (-100, 100) / 100f * GetAccuracy (),
						launcher.side);
					Zim.solid = false;
					Zim.playerSpeed = Vector2.zero;
					Timer.Create ("landmine", GetRange () / GetProjectileVelocity (), delegate {
						Zim.solid = true; 
						Zim.direction = Vector3.zero;
						return 0;
					});
					break;
				}
			}
			launcher.AddModifier ( new propertyModifier ("reload."+turretID,propertyModifier.Allignment_neutral, GetRecoilTime (), new int[]{ }), 0);

			Clip--;
		}
		if (Clip <= 0) {
			Reload (launcher,turretID);
			//launcher.ChangeWeapon();
		}
	}

	public void Reload(entityPlayer launcher,int turretID)
	{
		propertyModifier RelMod = new propertyModifier ("reload."+turretID,propertyModifier.Allignment_neutral, GetReloadTime ()*launcher.ModifierProperties[propertyModifier.property_reloadtime], new int[]{ }); 
		launcher.AddModifier (RelMod , 0);

		RelMod.AddFunction(propertyModifier.actions.expire,delegate {
			Clip=GetClipSize();
		});

	}

	public float GetNumProjectiles()
	{
		if (wType == WeaponType.land_Mines && level > 0) {
			return 4 + level * 2;
		} else if (wType == WeaponType.tank_cannon && level==0) {
			return 360f / (angleDeltaBase * GetProjectileSpreadMultiplier ());
		}
		return Mathf.Max(1,1+(level-1)*2);
	}

	public float GetRecoilTime()
	{
		if (wType == WeaponType.tank_cannon) {
			return 0.3f + level / 4f;
		} if (wType == WeaponType.land_Mines) {
			return .2f + .4f*level;
		} else if (level > 0) {
			return level;
		}

		return 0.15f+level;
	}

	public float GetRange()
	{
		if (wType == WeaponType.land_Mines) {
			if (level > 0) {
				return 20f;
			}
			return 1f;
		}
		if (wType == WeaponType.tank_cannon)
		{
			return 100f;
		}
		return 30f;
	}
	public float GetAccuracy()
	{
		if (wType == WeaponType.land_Mines) {
			if (level > 0) {
				return 12f;
			}
			return 0f;
		}
		if (wType == WeaponType.tank_cannon) {
			return 20f;
		}
		if (level == 0)
		{
			return 3f;
		}
			return 10;
	}

	public float GetProjectileVelocity()
	{
		if (wType == WeaponType.land_Mines) {
			return 80f;
		} else if (wType == WeaponType.tank_cannon) {
				return 18f;
			}
		if (level > 0) {
			return 60f;
		}
		return 40f;
	}

	public int GetClipSize()
	{
		if (wType == WeaponType.land_Mines) {
			if (level == 0) {
				return 4;
			}
			return 1;
		}
		if (level > 0) {
			return 3+level;
		}
		return 10;
	}

	public float GetReloadTime()
	{
		if (wType == WeaponType.land_Mines) {
			return 3+level;
		}
		if (level > 0) {
			return 1f;
		}
		return 0.4f;
	}

	public float GetDamageMultiplier()
	{
		if (wType == WeaponType.land_Mines) {
			return 10f;
		}
		if (wType == WeaponType.tank_cannon) {
			return 2f;
		}
		if (level > 0) {
			return 4f/level;
		}
		return 12f;
	}

	public float GetProjectileSpreadMultiplier()
	{

		if (wType == WeaponType.land_Mines) {
			return 1/2f;
		}
		if (wType == WeaponType.generic_Bullet) {
			return 1/4f;
		}
		return 1f;
	}

	public static propertyWeapon Custom(WeaponType gun, int upgrade){
		propertyWeapon Gun = new propertyWeapon ();

		Gun.wType = gun;
		Gun.level = upgrade;
		Gun.Clip = Gun.GetClipSize (); 

		return Gun;
	}
}


