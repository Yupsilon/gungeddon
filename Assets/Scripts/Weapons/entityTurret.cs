﻿using System;
using UnityEngine;

public class entityTurret:MonoBehaviour
	{
	public propertyWeapon wep;
	public float barrelRotation = 0f;
	public int ID = 0;

	public void Start()
	{
		if (wep.wType == propertyWeapon.WeaponType.generic_Bullet) {
			if (wep.level > 0) {
				gameObject.GetComponent<AudioSource> ().clip = util.LoadSound ("Sounds", "gunshot_large");
			} else {
				gameObject.GetComponent<AudioSource> ().clip = util.LoadSound ("Sounds", "gunshot_small");
			}
		} else if (wep.wType == propertyWeapon.WeaponType.tank_cannon) {
			gameObject.GetComponent<AudioSource> ().clip = util.LoadSound ("Sounds", "zap");
		}
		else if (wep.wType == propertyWeapon.WeaponType.land_Mines) {
			gameObject.GetComponent<AudioSource> ().clip = util.LoadSound ("Sounds", "landmine");
			GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Textures/Entities/Train_Unloader");
		}
		gameObject.GetComponent<AudioSource> ().playOnAwake = false;

	}
		
	public void Update()
	{
		transform.rotation = Quaternion.Euler (0, 0, barrelRotation);
	}

	public entityPlayer getOwner()
	{
		return gameObject.transform.parent.GetComponent<entityPlayer> ();
	}

	public Vector2 getForwardVector()
	{
		float angle = gameObject.transform.rotation.eulerAngles.z*Mathf.Deg2Rad;
		return new Vector2 ( Mathf.Cos (angle),Mathf.Sin (angle));
	}

	public void Fire()
	{

		if (getOwner().CanAttack () && !getOwner().HasModifier("reload."+ID)) {
			wep.Fire (getOwner(),(Vector2)gameObject.transform.position+getForwardVector()/2f, barrelRotation,ID);
			gameObject.GetComponent<AudioSource> ().PlayOneShot (gameObject.GetComponent<AudioSource> ().clip);
		}
	}
	}

