﻿using System;
using UnityEngine;

public class entityProjectile : MonoBehaviour
{
	Vector3 start = Vector3.zero;
	public Vector3 direction=Vector3.zero;
	public Vector2 playerSpeed=Vector2.zero;
	float damage=0;
	public bool solid=true;

	public float LifeTime = -1;

	entityPlayer caster;
    private void Start()
    {
		Rigidbody2D rbod = GetComponent<Rigidbody2D>();
		rbod.velocity = new Vector3(direction.x * direction.z + playerSpeed.x, direction.y * direction.z + playerSpeed.y, 0);
		gameObject.transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(rbod.velocity.y, rbod.velocity.x) * Mathf.Rad2Deg);

		GameObject.Destroy(gameObject, LifeTime );
	}
	
	public void LateUpdate ()
	{
		if (solid) {
			Vector2 vel = GetComponent<Rigidbody2D>().velocity;
			RaycastHit2D Data = Physics2D.Raycast (transform.position, vel, Time.fixedDeltaTime * .33f);

			if (Data.rigidbody != null) {
				entityPlayer other = Data.rigidbody.GetComponent<entityPlayer> ();
				if (other != null &&
				   other.TakeProjectiles () &&
				   !other.isAllied (caster)) {
					other.TakeDamage (damage);
					gameObject.transform.position = new Vector3 (Data.point.x, Data.point.y, gameObject.transform.position .z);
					Die(true);
				}
			} 
		}
	}

	public static entityProjectile LaunchProjectile(
		string prefab, entityPlayer launcher,
		Vector3 Pos, float time,float damage,
		float Vel, float Ang,
		Color32 col)
	{
		GameObject parent = GameObject.Instantiate(Resources.Load< GameObject>("Prefabs/Projectiles/"+prefab));
		parent.transform.position = Pos;
		parent.name = prefab;

		entityProjectile Launcher = parent.GetComponent<entityProjectile> ();

		Launcher.direction = new Vector3 (
			Mathf.Sin(Ang/180*Mathf.PI),
			Mathf.Cos(Ang/180*Mathf.PI),
			Vel
		);
		Launcher.playerSpeed = launcher.gameObject.GetComponent<Rigidbody2D> ().velocity;
		Launcher.caster = launcher;
		Launcher.LifeTime =  time;
		Launcher.damage = damage;

		parent.GetComponent<SpriteRenderer> ().color = col;

		return Launcher;
	}


	public void Die(bool explode)
	{
		if (explode)
		{
			GameObject boom = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/Projectiles/"+ gameObject.name + "_explode"));
			boom.transform.position = transform.position;

			if (boom.GetComponent<ParticleSystem>() !=null)
            {
				ParticleSystem.MainModule pmain = boom.GetComponent<ParticleSystem>().main;
				pmain.startColor = GetComponent<SpriteRenderer>().color;
			}

			if (boom.GetComponent<AudioSource> () != null)
			{
				boom.GetComponent<AudioSource>().PlayOneShot(boom.GetComponent<AudioSource>().clip);
			}
		}
		GameObject.DestroyImmediate (gameObject);
	}
}