﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicBackground : MonoBehaviour {

	Vector2 size;
	float currentaspect;
	System.Collections.Generic.List<GameObject> gameObjects;

	[SerializeField]
	public bool loopsX;
	public bool loopsY;
	public Vector2 speed=Vector2.zero;

	// Use this for initialization
	void Awake () {
		gameObject.tag = "Background";

			Texture2D floorT = gameObject.GetComponent<SpriteRenderer>().sprite.texture;

		gameObjects = new List<GameObject> ();

		size = new Vector2(floorT.width/20f,floorT.height/20f);
		Prepare (Camera.main.orthographicSize);
	}

	public void Prepare(float oSize)
	{
		Vector2 aspect = 
			new Vector2(Mathf.Ceil(Mathf.Max(oSize*Camera.main.aspect,oSize))*2,
				Mathf.Min(oSize*Camera.main.aspect,oSize)*2);

		Vector2Int vAspect = new Vector2Int ((int)aspect.x, (int)aspect.y);
		if (!loopsX) {
			vAspect.x = 1;
		}if (!loopsY) {
			vAspect.y = 1;
		}

		if (currentaspect != aspect.x) {
			currentaspect = aspect.x;

			gameObject.GetComponent<SpriteRenderer> ().size = new Vector2 ( vAspect.x*size.x*2,  vAspect.y*size.y*2);
			/*for (int iX = 0; iX < vAspect.x ; iX++) {
				for (int iY = 0; iY < vAspect.y; iY++) {
					GameObject Zim = new GameObject ();
					Zim.AddComponent<SpriteRenderer> ().sprite=gameObject.GetComponent<SpriteRenderer> ().sprite;
					Zim.transform.SetParent (transform);


					Zim.transform.localPosition = new Vector3 (
						(iX)*size.x*2,
						iY*size.y*2,
						0
					)-new Vector3(vAspect.x,vAspect.y,0)/4f*3f;

					gameObjects.Add (Zim);
				}
			}*/
		}
	}
	
	// Update is called once per frame
	void Update () {

	//	if (Time.time - lastUpdateTime > UpdateTime) 
		{

			Vector3 camPos = Camera.main.transform.position;

			Vector3 newPos = gameObject.transform.position;
			if (loopsX) {
				newPos.x = camPos.x-size.x - (camPos.x % (size.x * 2)) * speed.x;
			} else {
				newPos.x = camPos.x * speed.x;
			}
			if (loopsY) {
				newPos.y = camPos.y - (camPos.y % (size.y * 2)) * speed.y;
			} else {
				newPos.y = camPos.y  * speed.y;
			}

			gameObject.transform.position = newPos;
		}
	}
}
