﻿using UnityEngine;

public class Tire : MonoBehaviour {

	public float RestingWeight { get; set; }
	public float ActiveWeight { get; set; }
	public float Grip { get; set; }
	public float FrictionForce { get; set; }
	public float AngularVelocity { get; set; }
	public float Torque { get; set; }

	public float Radius = 0.5f;

	float lastDustTime=0;
	TrailRenderer Skidmark;

	Rigidbody2D body;
    private void Awake()
    {
		body = gameObject.transform.parent.GetComponentInParent<Rigidbody2D>();
		Skidmark = GetComponent<TrailRenderer>();
	}

    public void SetTrailActive(bool active) {
		Skidmark.emitting = active;

	}

}
