﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class gameInitializer:MonoBehaviour
	{
	public enum State
    {
		menu = 0,
		running = 1,
		gameover_death = 2,
		gameover_timer = 3,
		victory = 4,
	}
	public State gameState = State.menu;
	public float gameTime=0f;
	public static gameInitializer main;
	GUIStyle fontboxGUI;
	List<entityActor> cars = new List<entityActor>();

    public void Awake()
    {
		main = this;
    }
    public void Start()
	{
		fontboxGUI = new GUIStyle ();
		fontboxGUI.font = Resources.Load <Font>("GUI/BarwoodSCapsSSK-BoldItalic");
		fontboxGUI.fontSize = 20;

		makePlayer (Vector2.zero);
		ChangeGameState (State.menu);

			foreach (GameObject Zim in GameObject.FindGameObjectsWithTag("Background")) {
				if (Zim.GetComponent<DynamicBackground> () != null) {
					Zim.GetComponent<DynamicBackground> ().Prepare (40);

			}
		}
	}
	public void ChangeGameState(State newState)
	{
		gameState = newState;
		entityController gameplayer = GetGamePlayer().GetComponent<entityController>();
		if (newState == State.running) {

			gameplayer.enabled = true;

			gameTime = Time.time + 240f;

			gameplayer.playerInput = new Vector2 (0, 1);
			entityWagon Boss = entityWagon.CreateNew(GetGamePlayer().transform.position.y + Camera.main.orthographicSize);
		} else {
			gameplayer.enabled = false;
			if ((int)newState > (int)State.running) {
				gameplayer.boosting = false;
				gameplayer.breaking = false;
				gameplayer.playerInput = new Vector2 (0, 1);

				gameplayer.primaryfire = false;
				gameplayer.targetPoint = GetGamePlayer ().getForwardVector () * 10000 + (Vector2)GetGamePlayer ().transform.position;

				gameTime = Time.time + 3;
				if (newState == State.victory) {
					gameObject.GetComponent<AudioSource> ().PlayOneShot (util.LoadSound ("Sounds", "win"));
				} else {

					gameObject.GetComponent<AudioSource> ().PlayOneShot (util.LoadSound ("Sounds", "lose"));
				}
			}
		}
	}

	void OnGUI()
	{
		float mult = Screen.width / 1200f;
		switch (gameState) {
		case gameInitializer.State.menu:
			Texture2D tex = util.LoadTexture ("GUI", "Logo");
			GUI.DrawTexture (new Rect((Screen.width-tex.width*mult)/2f,tex.height/2f*mult,tex.width*mult,tex.height*mult),tex);
			break;
		case gameInitializer.State.running:
			float tTime = gameTime - Time.time;

			int min = Mathf.FloorToInt (tTime / 60);
			int sec = Mathf.FloorToInt (tTime - min * 60);

			string sSec = sec + "";
			if (sec < 10) {
				sSec = "0" + sec;
			}

			GUI.Box (new Rect (Screen.width  / 2f, 10f, 100, 40f), min+":"+sSec,fontboxGUI);
			break;
		case gameInitializer.State.gameover_timer:
			 tex = util.LoadTexture ("GUI", "Target_Lost");
			GUI.DrawTexture (new Rect((Screen.width-tex.width*mult)/2f,(Screen.height-tex.height*mult)/2f,tex.width*mult,tex.height*mult),tex);
			break;
		case gameInitializer.State.gameover_death:
			 tex = util.LoadTexture ("GUI", "Player_Dead");
			GUI.DrawTexture (new Rect((Screen.width-tex.width*mult)/2f,(Screen.height-tex.height*mult)/2f,tex.width*mult,tex.height*mult),tex);
			break;
		case gameInitializer.State.victory:
			 tex = util.LoadTexture ("GUI", "Win");
			GUI.DrawTexture (new Rect((Screen.width-tex.width*mult)/2f,(Screen.height-tex.height*mult)/2f,tex.width*mult,tex.height*mult),tex);
			break;
		}
	}

	 void Update()
	{
		entityActor player = GetGamePlayer ();
		switch (gameState) {
		case State.menu:
			float pos = Time.time * 10;
			entityCamera.main.issueCameraOrder (new Vector2 (-4, pos), 1, 0.5f,0,false);

			//Camera.main.orthographicSize = defCam;
			player.transform.position = new Vector3 (
				-7,
				pos - 2,
				-3
			);

			player.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 10);
			player.GetComponent<entityController> ().targetPoint = player.getForwardVector () + (Vector2)player.transform.position;

			if (Input.anyKey ) {
				ChangeGameState (State.running);
			}

			return;
		case State.running:
			if (gameTime < Time.time) {
				ChangeGameState (State.gameover_timer);
			}
			return;
		case State.gameover_death://defeat state player dead
		case State.gameover_timer://defeat state target lost
		case State.victory://victory state target destroyed
			entityCamera.main.issueCameraOrder (new Vector2 (player.transform.position.x, player.transform.position.y), 1.33f, 0.5f,0,false);

			if (Input.anyKey && gameTime < Time.time) {
				UnityEngine.SceneManagement.SceneManager.LoadScene ("GameScene");
			}
			return;
		}
	}

	public entityActor GetGamePlayer()
	{

		foreach (GameObject Zim in GameObject.FindGameObjectsWithTag ("Player")) {
			if (Zim.GetComponent<entityController> () != null) {
				return Zim.GetComponent<entityActor> ();
			}
		}
		return null;
	}

	public void makePlayer(Vector2 pos)
	{
		GameObject player = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/Cars/Player"));

		player.transform.position = new Vector3 (pos.x, pos.y, -3);
		player.GetComponentInChildren<entityTurret> ().wep = propertyWeapon.Custom (propertyWeapon.WeaponType.generic_Bullet, 0);
		cars.Add(player.GetComponent<entityActor>());
	}
	public void makeEnemy(Vector2 pos)
	{
		GameObject enemy = null;
		foreach (entityActor car in cars)
        {
			if (!car.isPlayer() && car.isDead())
            {
				car.Revive();
				enemy = car.gameObject;
            }
        }

		if (enemy == null)
		{
			enemy = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/Cars/Rival"));
				cars.Add(enemy.GetComponent<entityActor>());
		}

		enemy.transform.position = new Vector3 (pos.x, pos.y, -3);
		enemy.GetComponentInChildren<entityTurret> ().wep = propertyWeapon.Custom (propertyWeapon.WeaponType.generic_Bullet, UnityEngine.Random.Range(1,3));
	}
}