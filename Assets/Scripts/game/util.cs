﻿using System;
using UnityEngine;
	public static class util
	{


	public static Texture2D LoadTexture(string folder,string Dir)
	{

		if (Resources.Load<Texture2D> (folder+"/" + Dir ) != null) {
			Texture2D myTexture = Resources.Load<Texture2D> (folder+"/" + Dir );

			myTexture.filterMode = FilterMode.Point;

			return myTexture;
		}
		return null;
	}

	public static AudioClip LoadSound(string folder,string Dir)
	{
		if (Resources.Load<AudioClip> (folder+"/" + Dir) != null) {
			AudioClip myTexture = Resources.Load<AudioClip> (folder+"/" + Dir) ;


			return myTexture;
		}
		return null;
	}
}

