﻿using System;
using UnityEngine;
using UnityEngine.U2D;

public class entityCamera : MonoBehaviour
{
	public static entityCamera main;
	Camera cam;
	PixelPerfectCamera ppc;
	float originalScale = 1;
	float camScale = 1;
	Vector2 camOrder = Vector2.zero;
	float camsStartTime = 0;
	float cammStartTime = 0;
	float camScaleTime = 0;
	float camPosTime = 0;

    private void Awake()
    {
		cam = GetComponent<Camera>();
		ppc = GetComponent<PixelPerfectCamera>();
		main = this;
		originalScale = ppc.assetsPPU;
	}
    void Update()
	{
		if (camsStartTime + camScaleTime > Time.time)
		{
			float timeDelta = 1;
			if (camScaleTime > 0)
			{
				timeDelta = 1 / camScaleTime * Time.deltaTime;
			}
			ppc.assetsPPU = (int)(ppc.assetsPPU + (camScale - ppc.assetsPPU) * timeDelta);
		}
		if (cammStartTime + camPosTime > Time.time)
		{
			float timeDelta = 1;
			if (camPosTime > 0)
			{
				timeDelta = 1 / camPosTime * Time.deltaTime;
			}
			cam.transform.position = new Vector3(
				cam.transform.position.x + (camOrder.x - cam.transform.position.x) * timeDelta,
				cam.transform.position.y + (camOrder.y - cam.transform.position.y) * timeDelta,
				-10
			);
		}
	}


	public void issueCameraOrder(Vector2 pos, float oSize, float oTime, float mTime, bool forced)
	{
		oSize = Mathf.Clamp(oSize, .25f, .8f);

		issueCameraMoveOrder(pos, mTime);
		issueCameraScaleOrder(originalScale * oSize, oTime);
	}

	public void issueCameraMoveOrder(Vector2 pos, float mTime)
	{
		if (mTime == 0)
		{
			if (cammStartTime + camPosTime > Time.time)
			{

				camOrder = pos;
				camPosTime = cammStartTime + camPosTime - Time.time;
				cammStartTime = Time.time;
			}
			else
			{
				cam.transform.position = new Vector3(pos.x, pos.y, -10);
			}
		}
		else
		{
			camPosTime = mTime;
			camOrder = pos;
			cammStartTime = Time.time;

		}
	}

	public void issueCameraScaleOrder(float oSize, float oTime)
	{


		if (oTime == 0)
		{
			if (camsStartTime + camScaleTime > Time.time)
			{

				camScale = oSize;
				camPosTime = camsStartTime + camScaleTime - Time.time;
				camsStartTime = Time.time;
			}
			else
			{
				ppc.assetsPPU = (int)oSize;
			}
		}
		else
		{
			camScale = oSize;
			camsStartTime = Time.time;
			camScaleTime = oTime;
		}
	}
}