﻿using System;
using System.Collections.Generic;
using UnityEngine;

	public class TrainController : MonoBehaviour
{
	public int StartingWagons;
	public int SpawnedEnemies;
	System.Collections.Generic.List<entityWagon> train = new System.Collections.Generic.List<entityWagon>();
	entityActor enemyPlayer;
	bool camFollowPlayer;

	public void Start()
	{
		train.Add(GetComponent<entityWagon>());
		for (int Zim = 1; Zim < StartingWagons; Zim++) {

			foreach (entityWagon Gir in train) {
				entityPlayer Gert = Gir.GetComponent<entityPlayer> ();
				Gert.AddModifier(new propertyModifier("Train "+Zim,0,Mathf.Infinity,new int[]{},new float[][]{
					new float[]{propertyModifier.property_incomingdamage,.6f}
				}),0);
			}

			entityWagon parent = train[0];
			parent = entityWagon.CreateNew ( parent);
			parent.name = "Train " + Zim;
			parent.ID = train.Count;
			parent.GetComponent<AudioSource> ().PlayDelayed(Zim*0.2f);

			train.Insert(0,parent);
		}
		foreach (entityWagon Gir in train)
		{
			Gir.controller = this;
			Gir.InitWeapons();
		}
		enemyPlayer = gameInitializer.main.GetGamePlayer();
		camFollowPlayer =true;
		SpawnEnemy();
	}

    void Update()
	{
		GetComponent<Rigidbody2D>().velocity = new Vector2(0, 16);
		//Turret Control
		List<entityTurret> heads = new List<entityTurret>();
		foreach (entityWagon Gir in train)
		{
			heads.AddRange(Gir.mountedTurrets);
		}
		foreach (entityTurret Zim in heads) {
			if (Zim.wep.wType == propertyWeapon.WeaponType.land_Mines) {
				Zim.barrelRotation = 90;
				Zim.Fire ();
			} else {
				Vector2 turretOrientation = enemyPlayer.transform.position - Zim.transform.position;
				Zim.barrelRotation = Mathf.Atan2 (turretOrientation.y, turretOrientation.x)*Mathf.Rad2Deg;

				if (turretOrientation.sqrMagnitude < 1000) {
					Zim.Fire ();
				}
			}
		}
		//Camera
		if (gameInitializer.main.gameState == gameInitializer.State.running) {
			
			entityWagon wagon = null;

			foreach (entityWagon twagon in train) {
				if (wagon == null || wagon.hasDied ()) {
					wagon = twagon;
				}
			}

			if (wagon==null)
			{
				gameInitializer.main.ChangeGameState(gameInitializer.State.victory);//win
				return; 
			}

				float camSize = 12f;
			float transition = 0f;
			//game.issueCameraOrder (enemyPlayer.transform.position,Camera.main.orthographicSize, 0,0, true);

			if (enemyPlayer.transform.position.y < wagon.transform.position.y) {
				camSize =  2/(1+(enemyPlayer.transform.position - wagon.transform.position).magnitude);

				if (camFollowPlayer) {
					transition = 1/4f;
					camFollowPlayer = false;
				}

				entityCamera.main.issueCameraOrder ((enemyPlayer.transform.position + wagon.transform.position)/2f,camSize, 1, transition,false);
			} else {
				camSize =  Mathf.Abs(enemyPlayer.transform.position.x - wagon.transform.position.x);
				if (!camFollowPlayer) {
					transition = 1/4f;
					camFollowPlayer = true;
				}
			}
			if (camSize > 2 && Time.time- gameInitializer.main.gameTime<3) {
				entityCamera.main.issueCameraOrder (new Vector2 (enemyPlayer.transform.position.x, enemyPlayer.transform.position.y) - new Vector2 (enemyPlayer.transform.position.x,0).normalized, 1, 0.5f, 0, false);
				 } else {
				entityCamera.main.issueCameraOrder (enemyPlayer.transform.position, camSize, 1, transition,false);
			}
			if ((enemyPlayer.transform.position - wagon.transform.position).sqrMagnitude > 20000)
			{
				gameInitializer.main.ChangeGameState(gameInitializer.State.gameover_timer);
			}
		}
	}

	public void onWagonDeath(int ID)
	{
		foreach (entityWagon Zim in train) {
			if (Zim.next!=null && Zim.next.hasDied ()) {
				Zim.GetComponent<entityPlayer> ().Die ();
			} else {
				Zim.GetComponent<entityPlayer> ().RemoveModifierByName ("Train " + ID);
			}
		}
		for (int I = 0; I< SpawnedEnemies; I++)
        {
			SpawnEnemy();
		}
		train.RemoveAll (delegate(entityWagon obj) {
			return obj == null || obj.hasDied();
		});
		if (ID == 0) {
			gameInitializer.main.ChangeGameState (gameInitializer.State.victory);//win
		}
		camFollowPlayer=!camFollowPlayer	;
	}
	public void SpawnEnemy()
	{
		gameInitializer.main.makeEnemy(new Vector2(enemyPlayer.transform.position.x, enemyPlayer.transform.position.y - Camera.main.orthographicSize * 5));
	}
	}

