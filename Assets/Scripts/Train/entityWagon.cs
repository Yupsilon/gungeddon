﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class entityWagon : MonoBehaviour
{
	public int ID=0;
	public bool IsHead=false;
	int nTurrets = 2;
	public entityWagon next;
	public List<entityTurret> mountedTurrets = new List<entityTurret>();
	public TrainController controller;
	GameObject SmokeParticle;

	public static entityWagon CreateNew(float y)
	{
		GameObject prefab = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/Boss/Locomotive"));
		entityWagon wagonComp = prefab.GetComponent<entityWagon>();

			prefab.transform.position = new Vector3(
				0,
				y + 10 * prefab.GetComponent<TrainController>().StartingWagons,
				-3
			);
		
		return wagonComp;
	}

	public static entityWagon CreateNew(entityWagon parent)
	{
		GameObject prefab = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/Boss/Train"));
		entityWagon wagonComp = prefab.GetComponent<entityWagon>();

			prefab.transform.position = new Vector3(
				parent.transform.position.x,
				parent.transform.position.y - prefab.GetComponent<BoxCollider2D>().size.y,
				-3
			);
			prefab.GetComponent<FixedJoint2D>().enabled = true;
			prefab.GetComponent<FixedJoint2D>().connectedBody = parent.GetComponent<Rigidbody2D>();
			prefab.GetComponent<FixedJoint2D>().breakForce = Mathf.Infinity;
		
		wagonComp.next = parent;

		return wagonComp;

	}
	public void InitWeapons()
	{
		for (int Zim = 0; Zim < nTurrets; Zim++)
		{

			GameObject turretBase = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/Projectiles/Gun"));
			turretBase.transform.parent = transform;
			turretBase.transform.localPosition = new Vector3(0, ((float)(nTurrets - 1) / 2f - Zim) * GetComponent<BoxCollider2D>().size.y * 2 / 3f, -1);

			entityTurret Turret = turretBase.GetComponent<entityTurret>();
			Turret.ID = Zim;
			if (IsHead && Zim == 0)
			{
				Turret.wep = propertyWeapon.Custom(propertyWeapon.WeaponType.land_Mines, 1);
			}
			else if (Zim == 1 && ID == controller.StartingWagons-1)
			{

				Turret.wep = propertyWeapon.Custom(propertyWeapon.WeaponType.land_Mines, 0);
			}
			else
			{
				if (IsHead)
				{
					Turret.wep = propertyWeapon.Custom(propertyWeapon.WeaponType.tank_cannon, 0);
				}
				else
				{
					Turret.wep = propertyWeapon.Custom(propertyWeapon.WeaponType.tank_cannon, UnityEngine.Random.Range(1, 3));

				}
			}

			mountedTurrets.Add(Turret);
		}
	}

    public bool hasDied()
	{
		return  (gameObject.GetComponent<entityPlayer> ().isDead ()); 
	}

	public void Update()
	{

		entityPlayer player = gameObject.GetComponent<entityPlayer> ();

		if (player.Life < player.MaxLife * .33f)
		{
			if (SmokeParticle == null || SmokeParticle.name != "SmokeDarkLarge")
			{
				if (SmokeParticle != null)
				{
					GameObject.Destroy(SmokeParticle);
				}
				SmokeParticle = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/Effects/SmokeDarkLarge"));
			SmokeParticle.transform.SetParent(transform);
			SmokeParticle.transform.localPosition = Vector3.zero;
			SmokeParticle.transform.localRotation = Quaternion.Euler(0, 0, 0);
				SmokeParticle.transform.localScale = Vector3.one;
				SmokeParticle.name = "SmokeDarkLarge";
			}
		}
		else if (player.Life < player.MaxLife * .66f)
		{
			if (SmokeParticle == null || SmokeParticle.name != "SmokeLarge")
			{
				if (SmokeParticle != null)
				{
					GameObject.Destroy(SmokeParticle);
				}

				SmokeParticle = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/Effects/SmokeLarge"));
			SmokeParticle.transform.SetParent(transform);
			SmokeParticle.transform.localPosition = Vector3.zero;
			SmokeParticle.transform.localRotation = Quaternion.Euler(0, 0, 0);
                SmokeParticle.transform.localScale = Vector3.one;
				SmokeParticle.name = "SmokeLarge";
			}
		}
	} 
	public float lastSmokeParticleTime=0f;

	public void Die ()
	{
		if (ID >= 0)
		{
			controller.onWagonDeath(ID);
			ID = -1;

			Texture2D trashTexture = util.LoadTexture ("Entities", name + "_Body_Trashed");
			GetComponent<Rigidbody2D>().gravityScale = .2f;
			GetComponent<Rigidbody2D>().drag = 1;
			GameObject.Destroy (gameObject.GetComponent<FixedJoint2D> ());

			GameObject boom = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/Effects/Boss Death Explosion"));
			boom.transform.SetParent(transform);
			boom.transform.localPosition = Vector3.back * 6;
			boom.transform.localRotation = Quaternion.Euler(0, 0, 0);
			GameObject.Destroy(gameObject, 30);
		}
	}
}