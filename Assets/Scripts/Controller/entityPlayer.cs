﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class entityPlayer:MonoBehaviour {

	public List<propertyModifier> Modifiers = new List<propertyModifier>();
	public List<int> ModifierStates= new List<int>();
	public List<float> ModifierProperties= new List<float>();

	bool dead = false;
	public Color side = Color.white;
	public float Life=100;
	public float MaxLife=100;
	public int allignment=0;

	public static float flashTime = .15f;
	Color damageColor = Color.white;
	public float LastDamageTime = 0;
	SpriteRenderer sprite;

	public void Awake ()
	{
		sprite = GetComponent<SpriteRenderer>();
		UpdateStates ();
	}

	public void Start()
	{
		Life = MaxLife;
	}

	public void Revive()
	{
		Life = MaxLife;
		dead = false;
		UpdateStates();
	}

	public bool isAllied(entityPlayer other)
	{
		return other.allignment == this.allignment;
	}

	public void AddModifier( propertyModifier oData, int Type){

		oData.Duration = Time.time+oData.totDuration;

		if (oData.totDuration <= 0) {
			oData.Duration = Time.time;
		}
		switch (Type) {
		case 0: //Multiple 
			Modifiers.Add (oData);
			break;
		case 1: //Replace 
			RemoveModifierByName(oData.ModifierName);
			Modifiers.Add (oData);
			break;
		case 2: //Unique 
			if (!HasModifier (oData.ModifierName)) {
				Modifiers.Add (oData);
			}
			break;
		}
		UpdateStates ();
	}
	public void RemoveModifierByName(string Name){

		List<propertyModifier> Temp = new List<propertyModifier>();
		foreach (propertyModifier Mod in Modifiers) {
			if (Mod.ModifierName == Name) {
				Temp.Add (Mod);
			}
		}
		foreach (propertyModifier Mod in Temp) {
			Mod.Die (true);
			Modifiers.Remove (Mod);
		}
		UpdateStates ();
	}
	public bool HasModifier(string Name){
		return GetModifierByName (Name) != null;
	}

	public propertyModifier GetModifierByName(string Name){
		foreach (propertyModifier Mod in Modifiers) {
			if (Mod.ModifierName == Name) {return Mod;
			}
		}
		return null;
	}

	public void UpdateStates()
	{
		ModifierStates.Clear();
		ModifierProperties.Clear ();
		for (int i = 0; i < propertyModifier.property_max; i++) {
			ModifierProperties.Add (1f);
		}

		foreach (propertyModifier Mod in Modifiers) {
			if (Mod.ModifierStates!=null){foreach (int i in Mod.ModifierStates) {
					if (!ModifierStates.Contains (i)) {
						ModifierStates.Add (i);
					}
				}
			}
			if (Mod.ModifierProperties!=null){
				foreach (float[] d in Mod.ModifierProperties) {

					ModifierProperties  [(int)d [0]] *= d [1];

				}
			}
		}
	}


	public void Update ()
	{
		List<propertyModifier> Temp = new List<propertyModifier>();
		foreach (propertyModifier Mod in Modifiers) {
			if (Mod.totDuration>0 && Mod.Duration <= Time.time) {
				Temp.Add (Mod);
			}
		}
		foreach (propertyModifier Mod in Temp) {

			Mod.Die(true);
			Modifiers.Remove (Mod);
		}
		if (Temp.Count > 0) {
			UpdateStates ();
		}
		UpdateColor();
	}
	public void UpdateColor()
    {
		if (dead)
        {
		damageColor	= Color.gray;
        }
		else
        {
			if (Time.time>LastDamageTime)
            {
				damageColor = Color.white;

			}
			else
            {
				float delta = (LastDamageTime - Time.time) / flashTime;
				damageColor.r =  1;
				damageColor.g = 1 - delta;
				damageColor.b = 1 - delta;

			}
        }
		sprite.color = damageColor;

	}
	public bool CanAct(){

		return !ModifierStates.Contains(propertyModifier.modstate_Stunned) && !isDead();

	}
	public bool CanAttack(){
		return !ModifierStates.Contains (propertyModifier.modstate_Disarmed);}


	public bool TakeProjectiles()
	{return !isDead() && !ModifierStates.Contains(propertyModifier.modstate_Phased);}

	public bool DoesCollide ()
	{
		return !ModifierStates.Contains(propertyModifier.modstate_Phased);
	}

	public void TakeDamage(float Damage)
	{
		if (!ModifierStates.Contains(propertyModifier.modstate_Invulnerable)){
			 Damage*=ModifierProperties[propertyModifier.property_incomingdamage];
			Life -= Damage;
			LastDamageTime = Time.time + flashTime;
		}

		if (isDead()) {
				Die ();
			}
	}

	public bool isDead()
	{
		return Life <= 0;
	}
	public void Die()
	{
		if (!dead) {
			dead = true;
			Life = 0;
			if (gameObject.GetComponent<entityActor> () != null) {
				gameObject.GetComponent<entityActor> ().Die ();
			}
			if (gameObject.GetComponent<entityWagon> () != null) {
				gameObject.GetComponent<entityWagon> ().Die ();
			}
		}
	}
}