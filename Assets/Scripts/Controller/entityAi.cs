﻿using System;
using UnityEngine;

	public class entityAi:entityController
{
	entityActor enemyPlayer;
	Engine engie;
	public float desiredAngle=0;
	int state =0;

	public void Awake()
	{
		engie = gameObject.transform.Find ("Engine").gameObject.GetComponent<Engine> ();
		enemyPlayer = gameInitializer.main.GetGamePlayer();
	}

	public override void PlayerInput()
	{
		playerInput.y = 1;

		state = 0;//track
		desiredAngle = 0;
		if (gameInitializer.main.gameState == gameInitializer.State.running) {
			Vector2 center = (Vector2)gameObject.transform.position;
			Vector2 point = (Vector2)enemyPlayer.gameObject.transform.position;

			desiredAngle = Mathf.Atan2 (
				center.y - point.y, center.x - point.x
			) * Mathf.Rad2Deg + 90;
			if ((point - center).magnitude < 20) {
				state = 1;//attack

				/*if (point.y < center.y) {
					desiredAngle += 20;
				}*/
			} else {
				state = 0;//track
			}
			targetPoint = point;
		} else if (gameInitializer.main.gameState == gameInitializer.State.victory) {
			if (!GetComponent<entityActor> ().isPlayer ()) {

				desiredAngle = 180;
			}
		}


		while (desiredAngle < 0) {
			desiredAngle += 360;
		}

		if (state == 2) {
			desiredAngle = 0;
		}

		float delta = Mathf.DeltaAngle (gameObject.transform.rotation.eulerAngles.z, desiredAngle);

		playerInput.x = 0;
		if (Mathf.Abs (delta) > 20) {
			if (delta < 0) {
				playerInput.x = 1;
			} else {
				playerInput.x = -1;
			}
		}

		primaryfire = false;
		engie.automatic = true;
		breaking = false;
		boosting = false;
			switch (state) {
		case 0:
			if (delta > 30) {
				engie.ShiftGear (1);
				if (gameObject.GetComponent<Rigidbody2D> ().velocity.magnitude > 4) {
					breaking = engie.GetRPM (GetComponent<Rigidbody2D> ()) > 2000;
				}
				engie.automatic = false;
			} else {
				boosting = true;
			}
				break;

		case 1:
			primaryfire = true;

			if (engie.CurrentGear > 1) {
				engie.ShiftGear(1);
				engie.automatic = false;
			}

				break;
			}

	}

}

