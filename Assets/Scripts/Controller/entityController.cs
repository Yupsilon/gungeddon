﻿using System;
using UnityEngine;

public class entityController:MonoBehaviour
	{
	public Vector2 playerInput = Vector2.zero;
	public bool breaking=false;
	public bool primaryfire=false;
	public bool boosting=false;
	public Vector2 targetPoint = Vector2.zero;

	public void Update()
	{
		PlayerInput ();
	}

	public virtual void PlayerInput()
	{
		playerInput.x = Input.GetAxis ("Horizontal");
		playerInput.y = Input.GetAxis ("Vertical");

		primaryfire=Input.GetAxis ("Fire1")!=0;
		 targetPoint = Camera.main.ScreenToWorldPoint (Input.mousePosition);

		breaking = Input.GetAxis ("Boost/Brake")<0;
		boosting = Input.GetAxis ("Boost/Brake")>0;
	}
}

