﻿using UnityEngine;
using System.Collections.Generic;

public class entityActor : MonoBehaviour {

	entityPlayer player;
	entityController ctrl;
	GameObject SmokeParticle;

	public float  CGHeight = 0.55f;

	public float  InertiaScale = 1f;

	public float  BrakePower = 12000;

	public float  EBrakePower = 15000;

	public float  WeightTransfer = 0.35f;

	public float  MaxSteerAngle = 0.75f;

	public float  CornerStiffnessFront = 4.0f;

	public float  CornerStiffnessRear = 4.2f;

	public float  AirResistance = 1.5f;

	public float  RollingResistance = 1.0f;

	public float  EBrakeGripRatioFront = 0.9f;

	public float  TotalTireGripFront = 6.0f;

	public float  EBrakeGripRatioRear = 0.4f;

	public float  TotalTireGripRear = 4.8f;

	public float  SteerSpeed = 2.5f;

	public float  SteerAdjustSpeed = 1f;

	public float  SpeedSteerCorrection = 300f;

	public float  SpeedTurningStability = 10f;

	public float  AxleDistanceCorrection = 2f;

	public float SpeedKilometersPerHour {
		get {
			return Rigidbody2D.velocity.magnitude * 18f / 5f;
		}
	}

	// Variables that get initialized via code
	float Inertia = 1;
	float WheelBase = 1;
	float TrackWidth = 1;

	// Private vars
	float HeadingAngle;
	float AbsoluteVelocity;
	float AngularVelocity;
	float SteerDirection;
	float SteerAngle;

	Vector2 Velocity;
	Vector2 Acceleration;
	Vector2 LocalVelocity;
	Vector2 LocalAcceleration;

	float Throttle;
	float Brake;
	float EBrake;

	Rigidbody2D Rigidbody2D;

	Axle AxleFront;
	Axle AxleRear;
	Engine Engine;
	entityTurret Turret;

	void Awake() {

		Rigidbody2D = GetComponent<Rigidbody2D> ();

		
			AxleFront = transform.Find ("AxleFront").GetComponent<Axle> ();
			AxleRear = transform.Find ("AxleRear").GetComponent<Axle> ();

			Engine = transform.Find ("Engine").GetComponent<Engine> ();
		
		ctrl=GetComponent<entityController> ();
		player=GetComponent<entityPlayer> ();
		Turret = GetComponentInChildren<entityTurret>();

		CalcCar();
	}

	public Vector2 getForwardVector()
	{
		float angle = gameObject.transform.rotation.eulerAngles.z*Mathf.Deg2Rad;
		return new Vector2 (Mathf.Sin (angle), Mathf.Cos (angle));
	}

    public void Revive()
    {
		player.Revive();
	}

    void OnCollisionEnter2D (Collision2D collision)
	{
		float damage = collision.relativeVelocity.magnitude / 10f;
		if (Rigidbody2D.velocity.magnitude > collision.otherRigidbody.velocity.magnitude) {

			Rigidbody2D.transform.GetComponent<entityPlayer> ().TakeDamage (damage*0.8f);
			collision.otherRigidbody.GetComponent<entityPlayer> ().TakeDamage (damage*1.2f);
		} else {
			Rigidbody2D.transform.GetComponent<entityPlayer> ().TakeDamage (damage*1.2f);
			collision.otherRigidbody.GetComponent<entityPlayer> ().TakeDamage (damage*0.8f);
		}
	}
	void CalcCar() {

		Velocity = Vector2.zero;
		AbsoluteVelocity = 0;
		GameObject CenterOfGravity=transform.Find ("CenterOfGravity").gameObject;

		// Dimensions
		AxleFront.DistanceToCG = Mathf.Abs(CenterOfGravity.transform.localPosition.y - AxleFront.transform.Find("Axle").transform.position.y);
		AxleRear.DistanceToCG = Mathf.Abs(CenterOfGravity.transform.localPosition.y - AxleRear.transform.Find("Axle").transform.position.y);
		// Extend the calculations past actual car dimensions for better simulation
		AxleFront.DistanceToCG *= AxleDistanceCorrection;
		AxleRear.DistanceToCG *= AxleDistanceCorrection;
			
		WheelBase = AxleFront.DistanceToCG + AxleRear.DistanceToCG;
		Inertia = Rigidbody2D.mass * InertiaScale;

		// Set starting angle of car
		Rigidbody2D.rotation = transform.rotation.eulerAngles.z;
		HeadingAngle = (Rigidbody2D.rotation + 90) * Mathf.Deg2Rad;

	}
	void Start() {
		
		AxleFront.Init (Rigidbody2D, WheelBase);
		AxleRear.Init (Rigidbody2D, WheelBase);

		TrackWidth = Mathf.Abs (AxleRear.TireLeft.transform.position.x - AxleRear.TireRight.transform.position.x);
	}

	void Update()
	{

		if (ctrl != null && player.CanAct())
		{

			// Handle Input
			Throttle = 0;
			Brake = 0;
			EBrake = 0;

			if (ctrl.playerInput.y > 0)
			{
				Throttle = 1;
			}
			else if (ctrl.playerInput.y < 0)
			{
				Throttle = -1;
			}
			if (ctrl.breaking)
			{
				EBrake = 1;
			}

			float steerInput = 0;
			if (ctrl.playerInput.x < 0)
			{
				steerInput = 1;
			}
			else if (ctrl.playerInput.x > 0)
			{
				steerInput = -1;
			}

			/*
			if (Input.GetKeyDown (KeyCode.A)) {
				Engine.ShiftUp();
			} else if (Input.GetKeyDown (KeyCode.Z)) {
				Engine.ShiftDown();
			}
			*/

			// Apply filters to our steer direction
			SteerDirection = SmoothSteering(steerInput);
			SteerDirection = SpeedAdjustedSteering(SteerDirection);

			// Calculate the current angle the tires are pointing
			SteerAngle = SteerDirection * MaxSteerAngle;

			// Set front axle tires rotation
			AxleFront.TireRight.transform.localRotation = Quaternion.Euler(0, 0, Mathf.Rad2Deg * SteerAngle);
			AxleFront.TireLeft.transform.localRotation = Quaternion.Euler(0, 0, Mathf.Rad2Deg * SteerAngle);

			if (Turret != null)
			{
				Vector2 turretOrientation = ctrl.targetPoint - (Vector2)Turret.transform.position;
				Turret.barrelRotation = Mathf.Atan2(turretOrientation.y, turretOrientation.x) * Mathf.Rad2Deg;

				if (ctrl.primaryfire)
				{
					Turret.Fire();
				}
				else if (Turret.wep.Clip != Turret.wep.GetClipSize())
				{
					Turret.wep.Reload(player, Turret.ID);
				}
			}
		}
		if (player != null)
		{
			gameObject.GetComponent<BoxCollider2D>().enabled = player.DoesCollide();
		}

		if (ctrl.playerInput.y != 0)
		{

			Engine.GetComponent<AudioSource>().volume = 0.6f + Mathf.Abs(Engine.CurrentGear / 10f);
		}
		else
		{
			Engine.GetComponent<AudioSource>().volume = 0;
		}

		// Calculate weight center of four tires
		// This is just to draw that red dot over the car to indicate what tires have the most weight
		Vector2 pos = Vector2.zero;
		if (LocalAcceleration.magnitude > 1f)
		{

			float wfl = Mathf.Max(0, (AxleFront.TireLeft.ActiveWeight - AxleFront.TireLeft.RestingWeight));
			float wfr = Mathf.Max(0, (AxleFront.TireRight.ActiveWeight - AxleFront.TireRight.RestingWeight));
			float wrl = Mathf.Max(0, (AxleRear.TireLeft.ActiveWeight - AxleRear.TireLeft.RestingWeight));
			float wrr = Mathf.Max(0, (AxleRear.TireRight.ActiveWeight - AxleRear.TireRight.RestingWeight));

			pos = (AxleFront.TireLeft.transform.localPosition) * wfl +
				(AxleFront.TireRight.transform.localPosition) * wfr +
				(AxleRear.TireLeft.transform.localPosition) * wrl +
				(AxleRear.TireRight.transform.localPosition) * wrr;

			float weightTotal = wfl + wfr + wrl + wrr;

			if (weightTotal > 0)
			{
				pos /= weightTotal;
				pos.Normalize();
				pos.x = Mathf.Clamp(pos.x, -0.6f, 0.6f);
			}
			else
			{
				pos = Vector2.zero;
			}
		}

		// Skidmarks
		if (Mathf.Abs(LocalAcceleration.y) > 18 || EBrake == 1)
		{
			AxleRear.TireRight.SetTrailActive(true);
			AxleRear.TireLeft.SetTrailActive(true);
		}
		else
		{
			AxleRear.TireRight.SetTrailActive(false);
			AxleRear.TireLeft.SetTrailActive(false);
		}

		// Automatic transmission
		Engine.UpdateAutomaticTransmission(Rigidbody2D);

		if (player.Life < player.MaxLife * .33f)
		{
			if (SmokeParticle == null || SmokeParticle.name != "SmokeDark")
			{
				if (SmokeParticle != null)
				{
					GameObject.Destroy(SmokeParticle);
				}
				SmokeParticle = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/Effects/SmokeDark"));
				SmokeParticle.transform.SetParent(transform);
				SmokeParticle.transform.localPosition = Vector3.back * 6;
				SmokeParticle.transform.localScale = Vector3.one;
				SmokeParticle.transform.localRotation = Quaternion.Euler(0, 0, 0);
				SmokeParticle.name = "SmokeDark";
			}
		}
		else if (player.Life < player.MaxLife * .66f)
		{
			if (SmokeParticle == null || SmokeParticle.name != "Smoke")
			{
				if (SmokeParticle != null)
				{
					GameObject.Destroy(SmokeParticle);
				}
				SmokeParticle = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/Effects/Smoke"));
				SmokeParticle.transform.SetParent(transform);
				SmokeParticle.transform.localPosition = Vector3.back * 6;
				SmokeParticle.transform.localScale = Vector3.one;
				SmokeParticle.transform.localRotation = Quaternion.Euler(0, 0, 0);
				SmokeParticle.name = "Smoke";
			}
        }
		else if(SmokeParticle != null)
				{
			GameObject.Destroy(SmokeParticle);
		}
	}
			
	void FixedUpdate() {

		// Update from rigidbody to retain collision responses
		Velocity = Rigidbody2D.velocity;
		HeadingAngle = (Rigidbody2D.rotation + 90) * Mathf.Deg2Rad;

		float sin = Mathf.Sin(HeadingAngle);
		float cos = Mathf.Cos(HeadingAngle);

		// Get local velocity
		LocalVelocity.x = cos * Velocity.x + sin * Velocity.y;
		LocalVelocity.y = cos * Velocity.y - sin * Velocity.x;

		// Weight transfer
		float transferX = WeightTransfer * LocalAcceleration.x * CGHeight / WheelBase;
		float transferY = WeightTransfer * LocalAcceleration.y * CGHeight / TrackWidth * 20;		//exagerate the weight transfer on the y-axis

		// Weight on each axle
		float weightFront = Rigidbody2D.mass * (AxleFront.WeightRatio * -Physics2D.gravity.y - transferX);
		float weightRear = Rigidbody2D.mass * (AxleRear.WeightRatio * -Physics2D.gravity.y + transferX);

		// Weight on each tire
		AxleFront.TireLeft.ActiveWeight = weightFront - transferY;
		AxleFront.TireRight.ActiveWeight = weightFront + transferY;
		AxleRear.TireLeft.ActiveWeight = weightRear - transferY;
		AxleRear.TireRight.ActiveWeight = weightRear + transferY;
			
		// Velocity of each tire
		AxleFront.TireLeft.AngularVelocity = AxleFront.DistanceToCG * AngularVelocity;
		AxleFront.TireRight.AngularVelocity = AxleFront.DistanceToCG * AngularVelocity;
		AxleRear.TireLeft.AngularVelocity = -AxleRear.DistanceToCG * AngularVelocity;
		AxleRear.TireRight.AngularVelocity = -AxleRear.DistanceToCG *  AngularVelocity;

		// Slip angle
		AxleFront.SlipAngle = Mathf.Atan2(LocalVelocity.y + AxleFront.AngularVelocity, Mathf.Abs(LocalVelocity.x)) - Mathf.Sign(LocalVelocity.x) * SteerAngle;
		AxleRear.SlipAngle = Mathf.Atan2(LocalVelocity.y + AxleRear.AngularVelocity,  Mathf.Abs(LocalVelocity.x));

		// Brake and Throttle power
		float activeBrake = Mathf.Min(Brake * BrakePower + EBrake * EBrakePower, BrakePower);
		float activeThrottle = (Throttle * Engine.GetTorque (Rigidbody2D)) * (Engine.GearRatio * Engine.EffectiveGearRatio);
		if (ctrl.boosting) {
			activeThrottle *= 1.25f;
		}

		// Torque of each tire (rear wheel drive)
		AxleRear.TireLeft.Torque = activeThrottle / AxleRear.TireLeft.Radius;
		AxleRear.TireRight.Torque = activeThrottle / AxleRear.TireRight.Radius;

		// Grip and Friction of each tire
		AxleFront.TireLeft.Grip = TotalTireGripFront * (1.0f - EBrake * (1.0f - EBrakeGripRatioFront));
		AxleFront.TireRight.Grip = TotalTireGripFront * (1.0f - EBrake * (1.0f - EBrakeGripRatioFront));
		AxleRear.TireLeft.Grip = TotalTireGripRear * (1.0f - EBrake * (1.0f - EBrakeGripRatioRear));
		AxleRear.TireRight.Grip = TotalTireGripRear * (1.0f - EBrake * (1.0f - EBrakeGripRatioRear));

		AxleFront.TireLeft.FrictionForce = Mathf.Clamp(-CornerStiffnessFront * AxleFront.SlipAngle, -AxleFront.TireLeft.Grip, AxleFront.TireLeft.Grip) * AxleFront.TireLeft.ActiveWeight;
		AxleFront.TireRight.FrictionForce = Mathf.Clamp(-CornerStiffnessFront * AxleFront.SlipAngle, -AxleFront.TireRight.Grip, AxleFront.TireRight.Grip) * AxleFront.TireRight.ActiveWeight;
		AxleRear.TireLeft.FrictionForce = Mathf.Clamp(-CornerStiffnessRear * AxleRear.SlipAngle, -AxleRear.TireLeft.Grip, AxleRear.TireLeft.Grip) * AxleRear.TireLeft.ActiveWeight;
		AxleRear.TireRight.FrictionForce = Mathf.Clamp(-CornerStiffnessRear * AxleRear.SlipAngle, -AxleRear.TireRight.Grip, AxleRear.TireRight.Grip) * AxleRear.TireRight.ActiveWeight;

	 	// Forces
		float tractionForceX = AxleRear.Torque - activeBrake * Mathf.Sign(LocalVelocity.x);
		float tractionForceY = 0;

		float dragForceX = -RollingResistance * LocalVelocity.x - AirResistance * LocalVelocity.x * Mathf.Abs(LocalVelocity.x);
		float dragForceY = -RollingResistance * LocalVelocity.y - AirResistance * LocalVelocity.y * Mathf.Abs(LocalVelocity.y);

		float totalForceX = dragForceX + tractionForceX;
		float totalForceY = dragForceY + tractionForceY + Mathf.Cos (SteerAngle) * AxleFront.FrictionForce + AxleRear.FrictionForce;

		//adjust Y force so it levels out the car heading at high speeds
		if (AbsoluteVelocity > 10) {
			totalForceY *= (AbsoluteVelocity + 1) / (21f - SpeedTurningStability);
		}

		// If we are not pressing gas, add artificial drag - helps with simulation stability
		if (Throttle == 0) {
			Velocity = Vector2.Lerp (Velocity, Vector2.zero, 0.005f);
		}
	
		// Acceleration
		LocalAcceleration.x = totalForceX / Rigidbody2D.mass;
		LocalAcceleration.y = totalForceY / Rigidbody2D.mass;

		Acceleration.x = cos * LocalAcceleration.x - sin * LocalAcceleration.y;
		Acceleration.y = sin * LocalAcceleration.x + cos * LocalAcceleration.y;

		// Velocity and speed
		Velocity.x += Acceleration.x * Time.deltaTime;
		Velocity.y += Acceleration.y * Time.deltaTime;

		AbsoluteVelocity = Velocity.magnitude;

		// Angular torque of car
		float angularTorque = (AxleFront.FrictionForce * AxleFront.DistanceToCG) - (AxleRear.FrictionForce * AxleRear.DistanceToCG);

		// Car will drift away at low speeds
		if (AbsoluteVelocity < 0.5f && activeThrottle == 0)
		{
			LocalAcceleration = Vector2.zero;
			AbsoluteVelocity = 0;
			Velocity = Vector2.zero;
			angularTorque = 0;
			AngularVelocity = 0;
			Acceleration = Vector2.zero;
			Rigidbody2D.angularVelocity = 0;
		}

		var angularAcceleration = angularTorque / Inertia;

		// Update 
		AngularVelocity += angularAcceleration * Time.deltaTime;

		// Simulation likes to calculate high angular velocity at very low speeds - adjust for this
		if (AbsoluteVelocity < 1 && Mathf.Abs (SteerAngle) < 0.05f) {
			AngularVelocity = 0;
		} else if (SpeedKilometersPerHour < 0.75f) {
			AngularVelocity = 0;
		}

		HeadingAngle += AngularVelocity * Time.deltaTime;
		Rigidbody2D.velocity = Velocity;

		Rigidbody2D.MoveRotation (Mathf.Rad2Deg * HeadingAngle - 90);

	} 
	public float lastSmokeParticleTime=0f;

	float SmoothSteering(float steerInput) {

		float steer = 0;

		if(Mathf.Abs(steerInput) > 0.001f) {
			steer = Mathf.Clamp(SteerDirection + steerInput * Time.deltaTime * SteerSpeed, -1.0f, 1.0f); 
		}
		else
		{
			if (SteerDirection > 0) {
				steer = Mathf.Max(SteerDirection - Time.deltaTime * SteerAdjustSpeed, 0);
			}
			else if (SteerDirection < 0) {
				steer = Mathf.Min(SteerDirection + Time.deltaTime * SteerAdjustSpeed, 0);
			}
		}

		return steer;
	}

	float SpeedAdjustedSteering(float steerInput) {
		float activeVelocity = Mathf.Min(AbsoluteVelocity, 250.0f);
		float steer = steerInput * (1.0f - (AbsoluteVelocity / SpeedSteerCorrection));
		return steer;
	}

	public void Die()
	{
		Throttle = 0;
		EBrake = 1;
		if (isPlayer ()) {
			gameInitializer.main.ChangeGameState (gameInitializer.State.gameover_death);//gamestate defeat
		}
		GameObject boom = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/Effects/Car Death Explosion"));
		boom.transform.SetParent(transform);
		boom.transform.localPosition = Vector3.back * 6;
		boom.transform.localRotation = Quaternion.Euler(0,0,0);
	}

	public bool isPlayer()
	{
		return gameObject.tag == "Player";
	}

	public bool isDead()
	{
		return player.isDead();
	}
}
