# Gungeddon #

Bullet Chasers is a top-down twin stick shooter/bullet hell game, where the main objective is to destroy the train before it gets away, while fending of it's defenders, in this high speed, high intensity chase. Based of Jogallant's 2D car simulation.

Made for Unity 2D Challange 2018 - connect.unity.com/p/bullet-chasers

Download and play directly at: https://ypsilon.itch.io/bullet-chasers

# Installation #

To install, simply place all the files in a new Unity project. Recommended any 2019 Unity versions.
